all: main

main:
	pdflatex --synctex=1 main
	bibtex main
	pdflatex --synctex=1 main
	pdflatex --synctex=1 main

bibtex:
	bibtex main

clean:
	rm *.aux *.log *.bbl *.blg
